package HomeWork;

import java.util.Scanner;

public class LeapYear {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Enter the year to validat Leap year or not:");
		Scanner sc = new Scanner(System.in);
		int no = sc.nextInt();
		
		if (no%4==0 && no%100!=0){
			System.out.println(no + " is a leap year");
		}else if (no%400==0 && no%4==0) {
			System.out.println(no + " is a leap year");
		}else {
			System.out.println(no + " is not a leap year");
		}

	}

}
