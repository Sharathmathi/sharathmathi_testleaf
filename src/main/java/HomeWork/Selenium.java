package HomeWork;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Selenium {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		
		driver.get("http://leafground.com/pages/Dropdown.html");
		WebElement dropdown = driver.findElementById("dropdown1");
		Select dd = new Select(dropdown);
		dd.selectByIndex(3);
		
		WebElement dropdown2 = driver.findElementByName("dropdown2");
		Select dd2 = new Select(dropdown2);
		dd2.selectByVisibleText("Loadrunner");
		//driver.get("http://leafground.com/pages/checkbox.html");
		
		List<WebElement> allopt = dd.getOptions();
		int size = allopt.size() - 1;
		System.out.println(size);
		int i= 0;
		for (WebElement opt : allopt) {
			if(i==size) {
				opt.click();
			}
			i++;
		}

	}

}
