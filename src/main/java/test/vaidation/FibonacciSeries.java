package test.vaidation;

public class FibonacciSeries {
	
	public static void main(String[] args) {
		int n1 = 0;
		int n2 = 1;
		
		for (int i = 0; i < 11; i++) {
			int m = n1 + n2;
			n1 = n2;
			n2 = m;
			System.out.println(m);
		}
	}

}
