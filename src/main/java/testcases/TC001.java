package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
import wdMethods.SeMethods;

public class TC001 extends ProjectMethods{

	@Test(groups = {"smoke"})
	public void CreateLead() {
		//login();
		WebElement elecretealead = locateElement("xpath","//a[contains(text(),'Create Lead')]");
		click(elecretealead);
		WebElement eleCompNmae = locateElement("createLeadForm_companyName");
		type(eleCompNmae, "Cognizant");
		WebElement eleFrstName = locateElement("createLeadForm_firstName");
		type(eleFrstName, "Sahara");
		WebElement eleLastName = locateElement("createLeadForm_lastName");
		type(eleLastName, "Palani");
		WebElement eleLMarket = locateElement("createLeadForm_marketingCampaignId");
		selectDropDownUsingValue(eleLMarket, "CATRQ_CARNDRIVER");
		WebElement eleleadButton = locateElement("name","submitButton");
		click(eleleadButton);
		
		
	}
	
}












