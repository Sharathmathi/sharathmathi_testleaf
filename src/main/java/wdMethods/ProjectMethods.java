package wdMethods;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import week6.day2.LearnExl;

public class ProjectMethods extends SeMethods{
	
	@Parameters({"url", "username", "password"})
	@BeforeMethod(groups = {"smoke"})
	public void login(String url, String username, String password) {
		startApp("chrome", url);
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, username);
		WebElement elePassword = locateElement("password");
		type(elePassword, password);
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement eleCrmsfa = locateElement("text","CRM/SFA");
		click(eleCrmsfa);
	}
	@AfterMethod
	public void closeApp() {
		closeBrowser();
	}
	
	@DataProvider(name = "test")
		public Object[][] testData() throws IOException {
		Object[][] data = LearnExl.data();
			/*Object[][] data = new Object[2][5];
			data[0][0] = "Sharathmathi";
			data[0][1] = "Palani";
			data[0][2] = "CTS";
			data[0][3] = "Sharathmath@gmail.com";
			data[0][4] = 1234567890;
			
			
			data[1][0] = "Revathy";
			data[1][1] = "Adhimoolam";
			data[1][2] = "CG";
			data[1][3] = "Revathi@gmail.com";
			data[1][4] = 1123456789;*/
			return data;
		}
	
	@DataProvider(name = "qa")
	public Object[][] data() {
		Object[][] data = new Object[2][5];
		data[0][0] = "Sharathmathi";
		data[0][1] = "Palani";
		data[0][2] = "CTS";
		data[0][3] = "Sharathmath@gmail.com";
		data[0][4] = 1234567890;
		
		
		data[1][0] = "Revathy";
		data[1][1] = "Adhimoolam";
		data[1][2] = "CG";
		data[1][3] = "Revathi@gmail.com";
		data[1][4] = 1123456789;
		return data;
	}
	
}