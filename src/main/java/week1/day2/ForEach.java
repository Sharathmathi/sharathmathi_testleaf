package week1.day2;

import java.util.Scanner;

public class ForEach {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int sum = 0;
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the array size ");
		short arraySize = scan.nextShort();
	
		int nos[] = new int[arraySize];
		
		for (int i=0; i<10; i++) {
		System.out.println("The entry is ");
		nos[i] = scan.nextInt();	
		}
		
		for( int no : nos) {
			if (no%2 != 0) {
				System.out.println("The odd is "+no);
				sum = sum + no;
				
			}
		}
		
		System.out.println("The sum of odd nos "+sum);
	}

}
