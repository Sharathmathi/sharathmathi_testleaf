package week1.day2;

import java.util.Scanner;

public class SumofOdd {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner scan = new Scanner(System.in);

		int i = scan.nextInt();
		System.out.println(i);

		// int i = 35467;
		int sum = 0;
		String s = String.valueOf(i);

		for (char c : s.toCharArray()) {

			if (c % 2 != 0) {
				sum = sum + Character.getNumericValue(c);
				System.out.println(sum);
			}
		}

		System.out.println("Enter the no to find it is prime or not:");
		int no = scan.nextInt();
		boolean flag = false;

		for (int j = 2; j < no; j++) {
			if (no % j == 0) {
				flag = true;
				break;
			}
		}
		if (flag == true) {
			System.out.println(no + " is a not prime no");
		} else {
			System.out.println(no + " is a prime no");
		}
		
		System.out.println("Enter the no's to find the ");
		String nums = scan.next();
		int add = 0;
		
		String [] num = nums.split(",");
		
		for (String n : num) {
			boolean flg = false;
			int x = Integer.parseInt(n);	
			for(int y=2;y<x;y++) {
				if (x%y==0) {
					flg = true;
					break;
			}	
			}
			if (flg == false) {
				add = add + x;
				System.out.println(x);	
			}
			
		}
		System.out.println(add);
	}
}
