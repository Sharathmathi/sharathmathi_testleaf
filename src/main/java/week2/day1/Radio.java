package week2.day1;

public abstract class Radio {
	
	public void playstation() {
		System.out.println("playstation");
	}
	
	public abstract void changeBattery();
	
	public static void main(String[] args) {
		Radio rd = new Radio();
	}

}
