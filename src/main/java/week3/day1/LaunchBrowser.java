package week3.day1;

//import java.awt.List;

import org.openqa.selenium.WebElement;
//import org.openqa.selenium.Webelement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import java.awt.List;
import java.util.ArrayList;


public class LaunchBrowser {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		
		driver.get("http://leaftaps.com/opentaps/");
		
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		
		driver.findElementById("createLeadForm_companyName").sendKeys("CTS");
		driver.findElementById("createLeadForm_firstName").sendKeys("Sharathmathi");
		driver.findElementById("createLeadForm_lastName").sendKeys("Palani");
		
		//driver.findElementByClassName("smallSubmit").click();	
		
		WebElement source = driver.findElementById("createLeadForm_dataSourceId");
		Select dd = new Select(source);
		dd.selectByVisibleText("Public Relations");
		
		WebElement source1 = driver.findElementById("createLeadForm_marketingCampaignId");
		Select dd1 = new Select(source1);
		dd.selectByVisibleText("CATRQ_AUTOMOBILE");
		
		WebElement source2 = driver.findElementById("createLeadForm_industryEnumId");
		Select dd2 = new Select(source2);
		
		java.util.List<WebElement> options = dd2.getOptions();
		
	}
}
