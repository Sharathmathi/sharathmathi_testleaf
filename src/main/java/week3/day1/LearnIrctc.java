package week3.day1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class LearnIrctc {
	public static void main(String[] args) throws InterruptedException {
		
System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		
		driver.findElementById("userRegistrationForm:userName").sendKeys("Sharath");
		driver.findElementById("userRegistrationForm:password").sendKeys("mathi@123");
		driver.findElementById("userRegistrationForm:confpasword").sendKeys("mathi@123");
		WebElement SecQestion = driver.findElementById("userRegistrationForm:securityQ");
		Select SQ = new Select(SecQestion);
		SQ.selectByVisibleText("What is your pet name?");
		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("Sheelu");
		WebElement PreLang = driver.findElementById("userRegistrationForm:prelan");
		Select PL = new Select(PreLang);
		PL.selectByVisibleText("English");
		
		driver.findElementById("userRegistrationForm:firstName").sendKeys("Sharath");
        driver.findElementById("userRegistrationForm:middleName").sendKeys("mathi");
		driver.findElementById("userRegistrationForm:lastName").sendKeys("Palani");
		driver.findElementById("userRegistrationForm:gender:1").click();
		driver.findElementById("userRegistrationForm:maritalStatus:1").click();
		
		WebElement DObDay = driver.findElementById("userRegistrationForm:dobDay");
		Select DD = new Select(DObDay);
		DD.selectByVisibleText("03");
		WebElement dobMonth = driver.findElementById("userRegistrationForm:dobMonth");
		Select DM = new Select(dobMonth);
		DM.selectByVisibleText("DEC");
		WebElement dobbirth = driver.findElementById("userRegistrationForm:dateOfBirth");
		Select db = new Select(dobbirth);
		db.selectByVisibleText("1988");
		WebElement Occupation = driver.findElementById("userRegistrationForm:occupation");
		Select Oc = new Select(Occupation);
		Oc.selectByVisibleText("Private");
		
		driver.findElementById("userRegistrationForm:uidno").sendKeys("ABCD1234");
		driver.findElementById("userRegistrationForm:idno").sendKeys("bffpp3855g");
		
		WebElement Countries = driver.findElementById("userRegistrationForm:countries");
		Select CN = new Select(Countries);
		CN.selectByVisibleText("India");
		
		driver.findElementById("userRegistrationForm:email").sendKeys("adc123@gmail.com");
		//driver.findElementById("userRegistrationForm:isdCode").sendKeys("91");
		driver.findElementById("userRegistrationForm:mobile").sendKeys("9566211105");
		
		WebElement NationId = driver.findElementById("userRegistrationForm:nationalityId");
		Select NI = new Select(NationId);
		NI.selectByVisibleText("India");
		
		driver.findElementById("userRegistrationForm:address").sendKeys("C1,Phase2 Dakshin avenue");
        driver.findElementById("userRegistrationForm:street").sendKeys("Nagar");
		driver.findElementById("userRegistrationForm:area").sendKeys("Vandalur");
		driver.findElementById("userRegistrationForm:pincode").sendKeys("600021");
		//driver.findElementById("userRegistrationForm:statesName").sendKeys("Tamilnadu");
		driver.findElementById("userRegistrationForm:statesName").click();
		Thread.sleep(5000);
		WebElement CityName = driver.findElementById("userRegistrationForm:cityName");
		Select Ca = new Select(CityName);
		Ca.selectByVisibleText("Chennai");
		Thread.sleep(5000);
		WebElement PstOffice = driver.findElementById("userRegistrationForm:postofficeName");
		Select PO  = new Select(PstOffice);
		PO.selectByVisibleText("Washermanpet S.O");
		
		driver.findElementById("userRegistrationForm:landline").sendKeys("9566211105");
		driver.findElementById("userRegistrationForm:resAndOff:0").click();
	}

}
