package week3.day2;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebTable {
	public static void main(String[] args) throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		
		driver.get("https://erail.in");
		
		driver.findElementById("txtStationFrom").clear();
		driver.findElementById("txtStationFrom").sendKeys("TBM",Keys.TAB);
		driver.findElementById("txtStationTo").clear();
		driver.findElementById("txtStationTo").sendKeys("MDU",Keys.TAB);
		Thread.sleep(3000);
		
		driver.findElementById("chkSelectDateOnly").click();
		
		WebElement table = driver.findElementByXPath("//table[@class=DataTable TrainList']");
		List<WebElement> allrows = table.findElements(By.tagName("tr"));
		
		for (WebElement row : allrows) {
			System.out.println(row.findElements(By.tagName("td")).get(1).getText());
		}
		
	}
}
