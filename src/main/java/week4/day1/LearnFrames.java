package week4.day1;

import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnFrames {
	
	public static void main(String[] args) throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
					
		driver.switchTo().frame("iframeResult");
		driver.findElementByXPath("//button[contains(text(),'Try it')]").click();
		
		Alert alrt = driver.switchTo().alert();
		alrt.sendKeys("Sharathmathi");
		Thread.sleep(2000);
		alrt.accept();
		String text = driver.findElementById("demo").getText();
		boolean val =text.contains("Sharathmathi");
		
		if (val == true) {
			System.out.println("Success");
		}else{
			System.out.println("Failed");
		}
			}

}
