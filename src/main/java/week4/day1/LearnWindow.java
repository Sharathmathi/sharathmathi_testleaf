package week4.day1;

//import java.awt.List;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnWindow {

	public static void main(String[] args) throws InterruptedException, IOException {
		// TODO Auto-generated method st
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		
		driver.get("https://www.irctc.co.in/nget/train-search");
		Thread.sleep(3000);
		driver.findElementByXPath("//span[contains(text(),'AGENT LOGIN')]").click();
		driver.findElementByXPath("//a[contains(text(),'Contact Us')]").click();
		//System.out.println(driver.switchTo().window(nameOrHandle));
		File src = driver.getScreenshotAs(OutputType.FILE);
		File obj = new File("./Snaps/img1.jpeg");
		FileUtils.copyFile(src, obj);
		
		Set<String> allwnd = driver.getWindowHandles();
		List<String> lst = new ArrayList<>();
		lst.addAll(allwnd);
		String title = driver.switchTo().window(lst.get(0)).getTitle();
		String currentUrl = driver.switchTo().window(lst.get(0)).getCurrentUrl();
		System.out.println(title);
		System.out.println(currentUrl);
		
		//'for (String wind : lst) {
			//'wind.compareTo(anotherString);
		//}
		
	}

}
