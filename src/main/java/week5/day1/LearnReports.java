package week5.day1;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnReports {
	
	@Test
	public void report() throws IOException {
		ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/result.html");
		html.setAppendExisting(true);
		ExtentReports extent = new ExtentReports();
		extent.attachReporter(html);
		
		ExtentTest logger = extent.createTest("TC001_CreateLead","Create a New Lead");
		logger.assignAuthor("Sharat");
		logger.assignCategory("Smoke");
		
		logger.log(Status.PASS, "Enter the user id", MediaEntityBuilder.createScreenCaptureFromPath("./Snaps/img1.png").build());
		logger.log(Status.PASS, "Enter the password", MediaEntityBuilder.createScreenCaptureFromPath("./Snaps/img4.png").build());
		logger.log(Status.PASS, "Click the login bbutton", MediaEntityBuilder.createScreenCaptureFromPath("./Snaps/img5.png").build());
	
		extent.flush();
	}

}
