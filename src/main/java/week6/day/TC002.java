package week6.day;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
import wdMethods.SeMethods;

public class TC002 extends ProjectMethods{
	@Test(groups = {"sanity"})
	public void MegeLead() throws InterruptedException {
		//login();
//		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByXPath("//a[contains(text(),'Leads')]").click();
		driver.findElementByXPath("//a[contains(text(),'Merge Leads')]").click();
		driver.findElementByXPath("(//img[@src = '/images/fieldlookup.gif'])[1]").click();
		
		Set<String> windhand = driver.getWindowHandles();
		List<String> wind = new ArrayList<>();
		wind.addAll(windhand);
		driver.switchTo().window(wind.get(1));
		
		driver.findElementByName("firstName").sendKeys("Nagarajan");
		driver.findElementByXPath("//button[contains(text(),'Find Leads')]").click();
		Thread.sleep(3000);
		driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
		driver.switchTo().window(wind.get(0));
		driver.findElementByXPath("(//img[@src = '/images/fieldlookup.gif'])[2]").click();
		
		Set<String> windhand1 = driver.getWindowHandles();
		List<String> wind1 = new ArrayList<>();
		wind1.addAll(windhand1);
		driver.switchTo().window(wind1.get(1));
		
		System.out.println(driver.getTitle());
		Thread.sleep(3000);
		driver.findElementByName("firstName").sendKeys("Vandana");
		driver.findElementByXPath("//button[contains(text(),'Find Leads')]").click();
		Thread.sleep(1000);
		driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
		driver.switchTo().window(wind1.get(0));
		
		
		driver.findElementByClassName("buttonDangerous").click();
		Alert ale = driver.switchTo().alert();
		String text = ale.getText();
		System.out.println(text.contains("Are you sure?"));
		if (text.contains("Are you sure?")) {
			ale.accept();
		}
		
		driver.findElementByXPath("//a[contains(text(),'Find Leads')]").click();
		driver.findElementByName("id").sendKeys("abc");
		driver.findElementByXPath("//a[contains(text(),'Find Leads')]").click();

		
	}
	
}












