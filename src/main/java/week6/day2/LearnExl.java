package week6.day2;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.Test;

@Test
public class LearnExl {
	
	public static Object[][] data() throws IOException{
		XSSFWorkbook wb = new XSSFWorkbook("./data/createLead.xlsx");
		XSSFSheet ws = wb.getSheetAt(0);

		int lastRowNum =  ws.getLastRowNum();
		short lastCellNum = ws.getRow(0).getLastCellNum();
		Object[][] data = new Object[lastRowNum][lastCellNum];
		for(int i = 1;i<=lastRowNum;i++)
		{

			
			for (int j = 0; j < lastCellNum; j++) {
				try {
					String cellValue = ws.getRow(i).getCell(j).getStringCellValue();
					data[i-1][j] = cellValue;
					System.out.println(cellValue);
				} catch (NullPointerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}
		return data;
		
	}
	}

	// TODO Auto-generated method stub
