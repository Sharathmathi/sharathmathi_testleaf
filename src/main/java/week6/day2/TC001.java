package week6.day2;

import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
import wdMethods.SeMethods;

public class TC001 extends ProjectMethods{

	@Test(dataProvider= "test",groups = {"smoke"})
	public void CreateLead(String fname,String lname, String cname,String ename, String pname){
		//login();
		WebElement elecretealead = locateElement("xpath","//a[contains(text(),'Create Lead')]");
		click(elecretealead);
		
		WebElement eleFrstName = locateElement("createLeadForm_firstName");
		//type(eleFrstName, "Sahara");
		type(eleFrstName, fname);
		WebElement eleLastName = locateElement("createLeadForm_lastName");
		type(eleLastName, lname);
		WebElement eleCompNmae = locateElement("createLeadForm_companyName");
		type(eleCompNmae, cname);
		WebElement eleEmail = locateElement("createLeadForm_primaryEmail");
		type(eleEmail, ename);
		WebElement elePhno = locateElement("createLeadForm_primaryPhoneNumber");
		type(elePhno, pname);
		WebElement eleLMarket = locateElement("createLeadForm_marketingCampaignId");
		//selectDropDownUsingValue(eleLMarket, "CATRQ_CARNDRIVER");
		WebElement eleleadButton = locateElement("name","submitButton");
		//click(eleleadButton);
		
		
	}
	
}












