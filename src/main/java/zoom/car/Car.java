package zoom.car;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class Car extends ProjectMethods{
	@Test
	public void zoomCar() throws InterruptedException {
	login("chrome","https://wwww.zoomcar.com/chennai");
	Thread.sleep(3000);
	WebElement elesearch = locateElement("xpath","//a[@class='search']");
	click(elesearch);
	WebElement elepickuppnt = locateElement("xpath","//div[contains(text(),'Nelson Manickam Road')]");
	click(elepickuppnt);
	WebElement elenxt = locateElement("class","proceed");
	click(elenxt);
	WebElement elenxtdate = locateElement("xpath","//div[@class='day picked ']//following-sibling::div[1]");
	click(elenxtdate);
	click(elenxt);
	click(elenxt);
	//WebElement eledone = locateElement("xpath","//div[contains(text(),'Done']");
	
	List<WebElement> elements = driver.findElementsByClassName("price");
	List<WebElement> eletotalelements = new ArrayList<>();
	eletotalelements.addAll(elements);
	System.out.println(eletotalelements.size());
	int m = 0;
	for (WebElement webElement : eletotalelements) {
		//System.out.println(webElement.getText());	
		String text = webElement.getText().substring(2, 5);
		
		if(m  < Integer.parseInt(text)) {
			 m = Integer.parseInt(text);
			
			
			
		}
		
	String text2 = driver.findElementByXPath("//div[contains(text(),'"+m+"')]/../../preceding::h3[1]").getText();
	System.out.println("The car "+text2+" has the highest price of Rs "+m+".");
	driver.findElementByXPath("//div[contains(text(),'"+m+"')]/following-sibling::button").click();
	

}
}
}	