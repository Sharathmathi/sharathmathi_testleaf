package zoom.car;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.openqa.selenium.ElementNotSelectableException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;

public class SeMethods extends ExtentReport implements WdMethods{
	public int i = 1;
	public RemoteWebDriver driver;

	public void startApp(String browser, String url) {
		try {
			if (browser.equalsIgnoreCase("chrome")) {			
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				driver = new ChromeDriver();
			} else if (browser.equalsIgnoreCase("firefox")) {			
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			}
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			System.out.println("The Browser "+browser+" Launched Successfully");
		} catch (WebDriverException e) {
			System.out.println("The Browser "+browser+" not Launched ");
		} finally {
			takeSnap();			
		}
	}


	public WebElement locateElement(String locator, String locValue) {
		try {
			switch(locator) {
			case "id": return driver.findElementById(locValue);
			case "class": return driver.findElementByClassName(locValue);
			case "xpath": return driver.findElementByXPath(locValue);
			case "text": return driver.findElementByLinkText(locValue);
			case "name": return driver.findElementByName(locValue);
			//driver.findelementBy
			}
		} catch (NoSuchElementException e) {
			System.out.println("The Element Is Not Located ");
		}finally {
			takeSnap();
		}
		return null;
		
	}

	@Override
	public WebElement locateElement(String locValue) {
		return driver.findElementById(locValue);
	}

	@Override
	public void type(WebElement ele, String data) {
		ele.sendKeys(data);
		System.out.println("The Data "+data+" is Entered Successfully");
		takeSnap();
	}

	@Override
	public void click(WebElement ele) {
		ele.click();
		System.out.println("The Element "+ele+" Clicked Successfully");
		takeSnap();
	}

	@Override
	public String getText(WebElement ele) {
		String text = ele.getText();
		return text;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		Select dd = new Select(ele);
		dd.selectByVisibleText(value);
		System.out.println("The DropDown Is Selected with "+value);
	}
	
	@Override
	public void selectDropDownUsingValue(WebElement ele, String value) {
		try {
			Select dd = new Select(ele);
			dd.selectByValue(value);
			
		} catch (ElementNotSelectableException e) {
			// TODO Auto-generated catch block
			System.out.println("The DropDown Is Selected with "+value);
		}finally {
			takeSnap();
		}
	}


	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		// TODO Auto-generated method stub
		try {
			Select dd = new Select(ele);
			dd.selectByIndex(index);
			
		} catch (ElementNotSelectableException e) {
			// TODO Auto-generated catch block
			System.out.println("The DropDown Is Selected with "+index);
		}finally {
			takeSnap();
		}

	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		// TODO Auto-generated method stub
		try {
			String title = driver.getTitle();
			if (expectedTitle == title) {
				System.out.println("The title "+title+" is as expected");
				return true;
			}
			}catch (Exception e) {
				return false;
			}
			finally {
				takeSnap();
			}
			return false;
		}
		
	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		try {
			String text = ele.getText();
			if(expectedText==text) {
				System.out.println("The text"+text+"is as expected");
			}
		}catch(NoSuchElementException e) {
			
		}
		finally {
			takeSnap();
		}
	}
	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		try {
			String text = ele.getText();
			if(text.contains(expectedText)) {
				////logger.log(Status.PASS, "The expected text "+expectedText+" is part of the string "+text+"." );
			}
		}catch(NoSuchElementException e){
			//logger.log(Status.FAIL, "The expected text "+expectedText+" is part of the string "+text+"." );
		}

	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		try {
			String attribute2 = ele.getAttribute(value);
		}catch(NoSuchElementException e) {
			
			//logger.log(Status.FAIL, "The Attribute is not as expected");
		}

	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		try {
			String attribute2 = ele.getAttribute(value);
		}catch(NoSuchElementException e) {
			//logger.log(Status.FAIL, "The Attribute is not as expected");
		}
	}

	@Override
	public void verifySelected(WebElement ele) {
		// TODO Auto-generated method stub
		try {
			if (ele.isSelected() == true) {
				//logger.log(Status.PASS, "The element is selected" );
			}
		}catch(NoSuchElementException e) {
			//logger.log(Status.PASS, "The element is not selected" );
		}

	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub
		try {
			if (ele.isSelected() == true) {
				//logger.log(Status.PASS, "The element is displayed" );
			}
		}catch(NoSuchElementException e) {
			//logger.log(Status.PASS, "The element is not displayed" );
		}
	}

	@Override
	public void switchToWindow(int index) {
		Set<String> allWindows = driver.getWindowHandles();
		List<String> listOfWindow = new ArrayList<String>();
		listOfWindow.addAll(allWindows);
		driver.switchTo().window(listOfWindow.get(index));
		System.out.println("The Window is Switched ");
	}

	@Override
	public void switchToFrame(WebElement ele) {
		// TODO Auto-generated method stub
		driver.switchTo().frame(ele);
	}

	@Override
	public void acceptAlert() {
		driver.switchTo().alert().accept();
		
	}

	@Override
	public void dismissAlert() {
		// TODO Auto-generated method stub
		driver.switchTo().alert().dismiss();

	}

	@Override
	public String getAlertText() {
		// TODO Auto-generated method stub
		String text = driver.switchTo().alert().getText();
		return text;
	}

	@Override
	public void takeSnap() {
		try {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File dsc = new File("./snaps/img"+i+".png");
			FileUtils.copyFile(src, dsc);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		// TODO Auto-generated method stub
		driver.close();

	}

	@Override
	public void closeAllBrowsers() {
		// TODO Auto-generated method stub
		driver.close();
	}

}
